package net.luke.blelib.util

class BLEConstants {
    companion object {
        const val BLE_NOTIFICATION_DESCRIPTOR_UUID = "00002902-0000-1000-8000-00805f9b34fb"
        const val MAX_BLE_PACKET_SIZE = 20
    }
}