package net.luke.blelib.util

import android.util.Log
import androidx.annotation.IntDef
import net.luke.blelib.BuildConfig

class LogHelper {
    companion object {
        private val DEBUG : Boolean = BuildConfig.DEBUG
        private var prefix : String = ""

        fun init(prefix: String) {
            this.prefix = prefix
        }

        fun log(tag: String, message: String, @LogLevels level: Int = Log.DEBUG, forceLogInRelease: Boolean = false) {
            if(DEBUG || forceLogInRelease) {
                when(level) {
                    Log.ERROR -> {
                        Log.e("${prefix}__${tag}", message)
                    }
                    Log.WARN -> {
                        Log.w("${prefix}__${tag}", message)
                    }
                    Log.DEBUG -> {
                        Log.d("${prefix}__${tag}", message)
                    }
                    Log.INFO -> {
                        Log.i("${prefix}__${tag}", message)
                    }
                    Log.VERBOSE -> {
                        Log.v("${prefix}__${tag}", message)
                    }
                }
            }
        }
    }
}

@IntDef(Log.ERROR, Log.WARN, Log.DEBUG, Log.INFO, Log.VERBOSE)
@Retention(AnnotationRetention.SOURCE)
annotation class LogLevels