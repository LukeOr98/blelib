package net.luke.blelib.callbacks

interface ConnectionCallbacks {
    fun onDisconnected()
}