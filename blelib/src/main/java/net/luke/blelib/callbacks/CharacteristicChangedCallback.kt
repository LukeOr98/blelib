package net.luke.blelib.callbacks

import android.bluetooth.BluetoothGattCharacteristic

interface CharacteristicChangedCallback {
    fun onCharacteristicChanged(characteristic: BluetoothGattCharacteristic?)
}