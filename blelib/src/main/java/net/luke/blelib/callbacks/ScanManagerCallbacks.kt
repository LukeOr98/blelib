package net.luke.blelib.callbacks

import android.bluetooth.le.ScanResult
import net.luke.blelib.scan.ScanManager

internal interface ScanManagerCallbacks {
    fun onScanningChanged(sender: ScanManager, scanning: Boolean)
    fun onError(sender: ScanManager, message: String)
    fun onMatchFound(sender: ScanManager, result: ScanResult)
    fun onScanCompleted(sender: ScanManager)
}