package net.luke.blelib.comm

internal enum class OperationType {
    CONNECTION,
    DISCONNECTION,
    SERVICE_DISCOVERY,
    CHARACTERISTIC_WRITE,
    DESCRIPTOR_WRITE
}