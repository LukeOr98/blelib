package net.luke.blelib.comm

import android.bluetooth.*
import android.util.Log
import net.luke.blelib.callbacks.CharacteristicChangedCallback
import net.luke.blelib.callbacks.ConnectionCallbacks
import net.luke.blelib.util.LogHelper
import net.luke.blelib.util.TAG
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume

@Suppress("ObjectPropertyName")
internal object ConnectionManager {
    lateinit var continuation: Continuation<Boolean>
    lateinit var operationType: OperationType
    private var _isConnected: Boolean = false
    var isConnected: Boolean
        get() = _isConnected
        private set(value) {
            _isConnected = value
        }
    var characteristicChangedCallback: CharacteristicChangedCallback? = null
    var connectionCallbacks: ConnectionCallbacks? = null

    fun prepareOperation(cont: Continuation<Boolean>, opType: OperationType) {
        continuation = cont
        operationType = opType
    }

    val callbacks = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            when (newState) {
                BluetoothProfile.STATE_CONNECTED -> {
                    isConnected = true
                    if (operationType == OperationType.CONNECTION) {
                        LogHelper.log(TAG, "Device Connected")
                        continuation.resume(true)
                    }
                }
                BluetoothProfile.STATE_DISCONNECTED -> {
                    isConnected = false
                    if (operationType == OperationType.DISCONNECTION) {
                        LogHelper.log(TAG, "Device Disconnected")
                        continuation.resume(true)
                    } else {
                        LogHelper.log(TAG, "Device Unexpectedly Disconnected, Error", Log.ERROR)
                        connectionCallbacks?.onDisconnected()
                    }
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (operationType == OperationType.SERVICE_DISCOVERY) {
                LogHelper.log(TAG, "Services Discovered")
                continuation.resume(true)
            }
        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicWrite(gatt, characteristic, status)
            if (operationType == OperationType.CHARACTERISTIC_WRITE) {
                LogHelper.log(TAG, "Characteristic Written")
                continuation.resume(true)
            }
        }

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            super.onCharacteristicChanged(gatt, characteristic)
            LogHelper.log(TAG, "Characteristic Changed")
            characteristicChangedCallback?.onCharacteristicChanged(characteristic)
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int
        ) {
            super.onDescriptorWrite(gatt, descriptor, status)
            if (operationType == OperationType.DESCRIPTOR_WRITE) {
                LogHelper.log(TAG, "Descriptor Written")
                continuation.resume(true)
            }
        }
    }
}