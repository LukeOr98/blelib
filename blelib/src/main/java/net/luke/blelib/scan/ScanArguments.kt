package net.luke.blelib.scan

class ScanArguments(
    private var scanTimeInSeconds: Int,
    var scanAttempts: Int
) {
    fun getScanTimeInMillis(): Long {
        return scanTimeInSeconds * 1000L
    }
}