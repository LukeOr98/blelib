package net.luke.blelib.scan

import android.annotation.SuppressLint
import android.bluetooth.le.*
import android.os.Build
import android.util.Log
import kotlinx.coroutines.*
import net.luke.blelib.callbacks.ScanManagerCallbacks
import net.luke.blelib.util.LogHelper
import net.luke.blelib.util.TAG
import java.lang.ref.WeakReference

@OptIn(ObsoleteCoroutinesApi::class)
@Suppress("MemberVisibilityCanBePrivate")
@SuppressLint("ObsoleteSdkInt")
internal object ScanManager : ScanCallback() {

    //Scan Properties
    private var strategy: ((ScanResult, IScanParameters?) -> Unit)? = null
    private var params: IScanParameters? = null
    private var reason: String = ""
    private lateinit var scanCallbacks: WeakReference<ScanManagerCallbacks>
    private lateinit var leScanner: BluetoothLeScanner
    private lateinit var scanArguments: ScanArguments
    private lateinit var scanFilter: ScanFilter
    private var rssi: Int = -100
    private var scanPriority: ScanPriority = ScanPriority.NONE
    private var multiMatchPerDevice: Boolean = false

    //Scan Public APIs
    internal var matchCount: Int = 0
    internal var isScanning: Boolean = false

    //Scan Back Office Utilities
    private val scanSettings: ScanSettings
    private var attempts = 0
    private val knownAddresses = mutableListOf<String>()
    private val timeOutThread: ExecutorCoroutineDispatcher

    init {
        val settingsBuilder = ScanSettings.Builder().apply {
            setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
        }

        if (Build.VERSION.SDK_INT >= 23) {
            settingsBuilder.apply {
                setMatchMode(ScanSettings.MATCH_MODE_AGGRESSIVE)
                setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                setNumOfMatches(1)
            }
        }

        this.scanSettings = settingsBuilder.build()
        timeOutThread = newSingleThreadContext("Timeout")
    }

    //Public method used to start the scan
    internal fun startScan(
        scanStrategy: ((ScanResult, IScanParameters?) -> Unit)?,
        params: IScanParameters?,
        reason: String,
        callbacks: ScanManagerCallbacks,
        scanner: BluetoothLeScanner,
        arguments: ScanArguments,
        priority: ScanPriority,
        resetAttempts: Boolean = true,
        multiMatchPerDevice: Boolean = false,
        scanFilter: ScanFilter,
        rssiFilter: Int
    ) {
        if (isScanning) {
            if (priority == ScanPriority.HIGH && scanPriority == ScanPriority.LOW) {
                LogHelper.log(
                    TAG + "_startScan",
                    "Requested a scan (reason: $reason) that has higher priority. Aborting the running one (reason: ${this.reason})",
                    Log.WARN
                )
                stopScan(stopReason = "Priority Override", hasBeenStoppedOnPurpose = true)
            } else {
                LogHelper.log(
                    TAG + "_startScan",
                    "Cannot start scan (reason: $reason): Already scanning with higher or same priority (reason: ${this.reason})",
                    Log.WARN
                )
                return
            }
        }

        this.multiMatchPerDevice = multiMatchPerDevice
        this.scanPriority = priority
        this.strategy = scanStrategy
        this.params = params
        this.leScanner = scanner
        this.scanArguments = arguments
        this.scanCallbacks = WeakReference(callbacks)
        this.reason = reason
        this.scanFilter = scanFilter
        this.rssi = rssiFilter

        if (resetAttempts) {
            attempts = 0
        }

        leScanner.startScan(listOf(scanFilter), scanSettings, this)
        isScanning = true
        scanCallbacks.get()?.onScanningChanged(this, isScanning)
        LogHelper.log(
            TAG + "_startScan",
            "ScanManager Started Scanning - Current RSSI filter: $rssi - Reason: $reason"
        )
        CoroutineScope(timeOutThread).launch {
            delay(scanArguments.getScanTimeInMillis())
            attempts++
            stopScan("Timeout", true)
        }
    }

    //Public method used to dispose the current scan
    internal fun disposeCurrentScan(stopReason: String) {
        stopScan(stopReason = stopReason, hasBeenStoppedOnPurpose = true)
    }

    private fun resetCounters() {
        matchCount = 0
        knownAddresses.clear()
    }

    //Back Office Stop Scan Handler
    private fun stopScan(
        stopReason: String,
        timeout: Boolean = false,
        hasBeenStoppedOnPurpose: Boolean = false
    ) {
        LogHelper.log(TAG + "_stopScan", "Trying to stop scan. Reason: $stopReason", Log.WARN)
        if (isScanning) {
            stopBleScanner()
            when {
                timeout -> {
                    if (attempts < scanArguments.scanAttempts) {
                        restartScan()
                    } else {
                        LogHelper.log(TAG + "_stopScan", "Scan timed out", Log.WARN)
                        notifyScanSuccess()
                    }
                }
                hasBeenStoppedOnPurpose -> {
                    LogHelper.log(TAG + "_stopScan", "Scan Completed", Log.INFO)
                    notifyScanSuccess()
                }
                else -> {
                    LogHelper.log(TAG + "_stopScan", "Scan Error for reason $stopReason", Log.ERROR)
                    notifyScanError(stopReason)
                }
            }
        } else {
            LogHelper.log(TAG + "_stopScan", "No scan running. No action required", Log.WARN)
        }
    }

    private fun stopBleScanner() {
        LogHelper.log(TAG + "_stopScan", "Stopping BLE Scan")
        leScanner.stopScan(this)
        LogHelper.log(TAG + "_stopScan", "BLE Scan Stopped")
        isScanning = false
        scanCallbacks.get()?.onScanningChanged(this, isScanning)
    }

    private fun notifyScanError(stopReason: String) {
        scanCallbacks.get()?.onError(this, stopReason)
        resetCounters()
        scanPriority = ScanPriority.NONE
    }

    private fun notifyScanSuccess() {
        scanCallbacks.get()?.onScanCompleted(this)
        resetCounters()
        scanPriority = ScanPriority.NONE
    }

    private fun restartScan() {
        LogHelper.log(
            TAG + "_stopScan",
            "Attempt $attempts timed out, retrying",
            Log.WARN
        )
        resetCounters()
        startScan(
            scanStrategy = strategy,
            params = params,
            reason = reason,
            callbacks = scanCallbacks.get()!!,
            scanner = leScanner,
            arguments = scanArguments,
            priority = scanPriority,
            resetAttempts = false,
            scanFilter = scanFilter,
            rssiFilter = rssi
        )
    }

    private fun handleScanResult(sr: ScanResult) {
        matchCount++
        scanCallbacks.get()?.onMatchFound(this, sr)
        if (strategy != null) {
            (strategy as ((ScanResult, IScanParameters?) -> Unit)).invoke(
                sr,
                params
            )
        }
    }

    //This gets called whenever a scan result is found, we null check the result and invoke the Scan Strategy
    override fun onScanResult(callbackType: Int, result: ScanResult?) {
        result?.let { sr ->
            LogHelper.log(
                TAG + "_onScanResult",
                "Device found: ${sr.device.address} - rssi: ${sr.rssi} - reason: $reason",
                Log.VERBOSE
            )
            if (sr.rssi > rssi) {
                if (!multiMatchPerDevice) {
                    if (knownAddresses.contains(sr.device.address)) {
                        return@let
                    } else {
                        knownAddresses.add(sr.device.address)
                        handleScanResult(sr)
                    }
                } else {
                    handleScanResult(sr)
                }
            }
        }
    }

    //This gets called on every scan error.
    override fun onScanFailed(errorCode: Int) {
        if (attempts < scanArguments.scanAttempts) {
            LogHelper.log(TAG + "_onScanFailed", "BLE scan failed: $errorCode", Log.ERROR)
            stopScan(stopReason = "Scan Failed with Error $errorCode")
        }
    }
}