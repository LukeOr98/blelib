package net.luke.blelib.scan

enum class ScanPriority {
    HIGH,
    LOW,
    NONE
}