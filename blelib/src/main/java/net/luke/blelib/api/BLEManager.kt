package net.luke.blelib.api

import android.bluetooth.*
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.content.Context
import android.util.Log
import net.luke.blelib.callbacks.CharacteristicChangedCallback
import net.luke.blelib.callbacks.ConnectionCallbacks
import net.luke.blelib.callbacks.ScanManagerCallbacks
import net.luke.blelib.comm.ConnectionManager
import net.luke.blelib.comm.OperationType
import net.luke.blelib.scan.*
import net.luke.blelib.util.BLEConstants
import net.luke.blelib.util.LogHelper
import net.luke.blelib.util.TAG
import java.lang.Exception
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@Suppress("unused", "MemberVisibilityCanBePrivate")
object BLEManager {
    private lateinit var gatt: BluetoothGatt
    private var bleScanner: BluetoothLeScanner? = null

    val isConnected: Boolean
        get() = ConnectionManager.isConnected
    val isScanning: Boolean
        get() = ScanManager.isScanning

    fun init(bleScanner: BluetoothLeScanner) {
        this.bleScanner = bleScanner
    }

    suspend fun scan(
        scanStrategy: ((ScanResult, IScanParameters?) -> Unit)? = null,
        strategyParams: IScanParameters? = null,
        reason: String,
        arguments: ScanArguments,
        priority: ScanPriority,
        multiMatchPerDevice: Boolean,
        scanFilter: ScanFilter = ScanFilter.Builder().build(),
        rssi: Int
    ): List<ScanResult> {
        return if(!isConnected && !isScanning) {
            if (bleScanner == null) {
                LogHelper.log(TAG, "You should call 'init()' first!", Log.ERROR)
                return emptyList()
            }
            val results = mutableListOf<ScanResult>()
            suspendCoroutine { cont ->
                ScanManager.startScan(
                    scanStrategy = scanStrategy,
                    params = strategyParams,
                    reason = reason,
                    scanner = bleScanner!!,
                    callbacks = object : ScanManagerCallbacks {
                        override fun onScanningChanged(sender: ScanManager, scanning: Boolean) {
                            LogHelper.log(TAG, "Scanning changed to $scanning")
                        }

                        override fun onError(sender: ScanManager, message: String) {
                            try {
                                LogHelper.log(TAG, "Returning Scan Failed Result", Log.ERROR)
                                cont.resume(results.toList())
                            } catch (e: Exception) {
                                LogHelper.log(TAG, e.message!!, Log.WARN)
                            }
                        }

                        override fun onMatchFound(sender: ScanManager, result: ScanResult) {
                            results.add(result)
                        }

                        override fun onScanCompleted(sender: ScanManager) {
                            try {
                                LogHelper.log(TAG, "Returning Scan Completed Result", Log.WARN)
                                cont.resume(results.toList())
                            } catch (e: Exception) {
                                LogHelper.log(TAG, e.message!!, Log.WARN)
                            }
                        }
                    },
                    arguments = arguments,
                    priority = priority,
                    multiMatchPerDevice = multiMatchPerDevice,
                    scanFilter = scanFilter,
                    rssiFilter = rssi
                )
            }
        }
        else emptyList()
    }

    fun scanAsync(
        scanStrategy: ((ScanResult, IScanParameters?) -> Unit)?,
        params: IScanParameters,
        reason: String,
        arguments: ScanArguments,
        priority: ScanPriority,
        multiMatchPerDevice: Boolean,
        scanFilter: ScanFilter = ScanFilter.Builder().build(),
        rssi: Int,
        callbacks: ScanCallbacks
    ) {
        if(!isConnected && !isScanning) {
            if (bleScanner == null) {
                LogHelper.log(TAG, "You should call 'init()' first!", Log.ERROR)
                return
            }
            ScanManager.startScan(
                scanStrategy = scanStrategy,
                params = params,
                reason = reason,
                scanner = bleScanner!!,
                callbacks = object : ScanManagerCallbacks {
                    override fun onScanningChanged(sender: ScanManager, scanning: Boolean) {
                        LogHelper.log(TAG, "Scanning changed to $scanning")
                    }

                    override fun onError(sender: ScanManager, message: String) {
                        callbacks.onScanEnded(false, sender.matchCount)
                    }

                    override fun onMatchFound(sender: ScanManager, result: ScanResult) {
                        callbacks.onMatchFound(result)
                    }

                    override fun onScanCompleted(sender: ScanManager) {
                        callbacks.onScanEnded(true, sender.matchCount)
                    }
                },
                arguments = arguments,
                priority = priority,
                multiMatchPerDevice = multiMatchPerDevice,
                scanFilter = scanFilter,
                rssiFilter = rssi
            )
        }
    }

    fun disposeScan(reason: String) {
        if(isScanning) {
            ScanManager.disposeCurrentScan(reason)
        }
    }

    suspend fun connectToDevice(device: BluetoothDevice, ctx: Context, callbacks: ConnectionCallbacks): Boolean {
        LogHelper.log(TAG, "Start connectToDevice")
        return if(!isConnected && !isScanning) {
            suspendCoroutine {
                ConnectionManager.prepareOperation(it, OperationType.CONNECTION)
                ConnectionManager.connectionCallbacks = callbacks
                gatt = device.connectGatt(ctx, false, ConnectionManager.callbacks)
            }
        } else false
    }

    suspend fun disconnect(): Boolean {
        LogHelper.log(TAG, "Start disconnect")
        val result = if(isConnected && !isScanning) {
            suspendCoroutine {
                ConnectionManager.prepareOperation(it, OperationType.DISCONNECTION)
                gatt.disconnect()
            }
        }
        else false
        gatt.close()
        return result
    }

    suspend fun discoverServices(): List<BluetoothGattService> {
        LogHelper.log(TAG, "Start discoverServices")
        return if(isConnected && !isScanning) {
            suspendCoroutine<Boolean> {
                ConnectionManager.prepareOperation(it, OperationType.SERVICE_DISCOVERY)
                gatt.discoverServices()
            }
            gatt.services
        }
        else listOf()
    }

    suspend fun writeCharacteristic(characteristic: BluetoothGattCharacteristic): Boolean {
        LogHelper.log(TAG, "Start writeCharacteristic")
        return if(isConnected && !isScanning) {
            suspendCoroutine {
                ConnectionManager.prepareOperation(it, OperationType.CHARACTERISTIC_WRITE)
                gatt.writeCharacteristic(characteristic)
            }
        }
        else false
    }

    suspend fun subscribeToNotifications(
        characteristic: BluetoothGattCharacteristic,
        callbacks: CharacteristicChangedCallback
    ): Boolean {
        LogHelper.log(TAG, "Start subscribeToNotifications")
        return if(isConnected && !isScanning) {
            suspendCoroutine {
                LogHelper.log(TAG, "Preparing operation subscribeToNotifications")
                ConnectionManager.prepareOperation(it, OperationType.DESCRIPTOR_WRITE)
                ConnectionManager.characteristicChangedCallback = callbacks
                gatt.setCharacteristicNotification(characteristic, true)
                val descriptor =
                    characteristic.getDescriptor(UUID.fromString(BLEConstants.BLE_NOTIFICATION_DESCRIPTOR_UUID))
                descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                LogHelper.log(TAG, "Start writing descriptor subscribeToNotifications")
                gatt.writeDescriptor(descriptor)
            }
        }
        else false
    }

    suspend fun unSubscribeFromNotifications(characteristic: BluetoothGattCharacteristic): Boolean {
        LogHelper.log(TAG, "Start unSubscribeFromNotifications")
        return if(isConnected && !isScanning) {
            suspendCoroutine {
                LogHelper.log(TAG, "Preparing operation unSubscribeFromNotifications")
                ConnectionManager.prepareOperation(it, OperationType.DESCRIPTOR_WRITE)
                ConnectionManager.characteristicChangedCallback = null
                gatt.setCharacteristicNotification(characteristic, false)
                val descriptor = characteristic.getDescriptor(UUID.fromString(BLEConstants.BLE_NOTIFICATION_DESCRIPTOR_UUID))
                descriptor.value = BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE
                LogHelper.log(TAG, "Start writing descriptor unSubscribeFromNotifications")
                gatt.writeDescriptor(descriptor)
            }
        }
        else false
    }
}

interface ScanCallbacks {
    fun onMatchFound(result: ScanResult)
    fun onScanEnded(result: Boolean, count: Int)
}