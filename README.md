Will format this better later...

To add this project as a dependency add this repository in the project-level build.gradle

```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

and this implementation to the app-level build.gradle.

```
dependencies {
    implementation 'com.gitlab.LukeOr98:blelib:master-SNAPSHOT'
}

```